#include <NewPing.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

// Prototipos de funciones externas. Definidas en archivos asm
extern "C" {
  void start(); 
  void blink();
}

int distancia, encriptado;
String str;
char *cstr;
// Clave para la encriptacion
byte XOR_KEY = B01010101;

#define TRIG_PIN 5
#define ECHO_PIN 6
#define MAX_DISTANCE 400

// Lo primero is inicializar la librería indicando los pins de la interfaz
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);
RF24 radio(9, 10);

//Debug
int serial_putc( char c, FILE * )
{
  Serial.write( c );
  return c;
}

//Debug
void printf_begin(void)
{
  fdevopen( &serial_putc, 0 );
}

/*
 * Inicializacion de la interfaz serial y del módulo radiofrecuencia.*/
void setup()
{
  Serial.begin(9600);
  printf_begin();      //Debug
  radio.begin();
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(0x4c);
  radio.openWritingPipe(0xF0F0F0F0E1LL);
  radio.enableDynamicPayloads();
  radio.powerUp();
  radio.printDetails(); //Debug
  start();
}

/*
 * Ciclo de funcionamiento. Realiza el sensado, luego se encripta
 * y por último se transmite al Nodo_Receptor.*/
void loop()
{
  distancia = Sensado();
  Serial.println("Valor adquirido: " + String(distancia));
  encriptado = Encriptador(distancia);
  Serial.println("Valor encriptado: " + String(encriptado));
  delay(3000);
  Transmision(encriptado);
  Serial.println("");
}


/*-------------------------------SENSADO------------------------------------*/
/* Realiza el sensado del entorno */
int Sensado()
{
  int distancia = sonar.ping_cm();
  return distancia;
}

/*-------------------------------ENCRIPTADOR---------------------------------*/
/* Empleo de operacion logica a nivel de bits XOR para la encriptacion */
int Encriptador(int distancia)
{
  int XOR_out = (distancia ^ XOR_KEY);
  return XOR_out;
}


/*-------------------------------TRANSMISION------------------------------------*/
/* Transmision del dato encriptado. Determina si pudo ser realizada o no. En caso
 * de ser posible la transmisión, activa un Led para indicar de tal situacion. */
void Transmision(int valor)
{
  if (!radio.write(&encriptado, sizeof(int)))
  {
    Serial.println(F("FALLO!"));
  } else {
    Serial.println("ENVIADO: " + String(valor));
    blink();
  }
}


/*-------------------------------PRINT_S------------------------------------*/
void print_String (unsigned char *cadena)
{
  for (int y = 0; y < strlen((char *)cadena); y++)
  {
    Serial.print(cadena[y], HEX);
  }
  Serial.println("");
}
