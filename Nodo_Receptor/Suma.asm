.global suma, start
start:	@ldr sp, = stack_top
		bl main				@ call main() in c
stop:	b stop

@ Funcion que devuelve la suma de 5 valores enteros.
@ Equivalencia en C:
@ int suma( int a, int b, int c, int d, int e) 
@ { return a+b+c+d+e; }

suma:	
@ Al entrar la parte superior de la pila contiene a 'e' pasado como parametro
@ en la funcion main en Cpp.
@ Estableciendo el stack frame
@ fp: frame pointer.
@ lr: link register. Contiene la dirección a la que se debe volver cuando
@ 					 finaliza una llamada de función .

@			 ------- 
@ Init SP ->|		| #-8
@			|-------|
@	|		|	lr	| #-4
@	|		|-------|
@	|		|	fp	| #0     
@	|		|-------|
@	V		|	'e'	| #4	Final SP
@			|-------|
@			|	'f'	| #8
@			 -------

@ Empezando en la dirección SP-4*2 copia el contenido de los registros
@ FP y LR. SP queda con la dirección de comienzo (SP-4*2)

	stmfd sp!, {fp, lr}		@ push fp, lr
	add   fp, sp, #4		@ fp -> guardar registro lr en el stack
@ Realiza la suma de los 5 parametros
	add r0, r0, r1			@ Los primeros 4 parametros estan en los
	add r0, r0, r2			@ registros r0, r1, r2, r3. Se realiza la suma
	add r0, r0, r3			@ almacenando el resultado en r0
	ldr r3, [fp, #4]		@ Carga parametro 'e' dentro de r3
	add r0, r0, r3 			@ Realizo la suma y almaceno resultado en r0
@	ldr r3, [fp, #8]		@ Carga parametro 'f' dentro de r3
@	add r0, r0, r3 			@ Realizo la suma y almaceno resultado en r0
@ Retorno a la funcion llamante
	sub sp, fp, #4			@ sp = fp-4

@			 ------- 
@ 			|		| #-8 	Final SP
@			|-------|
@			|	pc	| #-4 
@			|-------|
@ 			|	fp	| #0 
@			|-------|
@ Init SP ->|	'e'	| #4
@			|-------|
@			|	'f'	| #8
@			 -------

@ Empezando en la dirección SP copia el contenido de la memoria en los
@ registros FP y PC. SP queda con la dirección original + 4*2	
	 
	ldmfd sp!, {fp, pc}		@ pop fp, pc. Retorno.
