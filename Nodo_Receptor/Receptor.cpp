/*
 Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.

 03/17/2013 : Charles-Henri Hallard (http://hallard.me)
              Modified to use with Arduipi board http://hallard.me/arduipi
						  Changed to use modified bcm2835 and RF24 library
TMRh20 2014 - Updated to work with optimized RF24 Arduino library

 */

/**
 * Codigo adaptado para implementar comunicacion unidireccional 
 * Cliente(Arduino) -> Servidor(Raspberry)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <RF24/RF24.h>
#include <time.h>

int check_module(void);
int parseInt(char*);
int powInt(int,int);
extern "C" int suma(int, int, int, int, int);

using namespace std;
//
// Configuracion del Hardware
//

/****************** Raspberry Pi ***********************/

// Radio CE Pin, CSN Pin, SPI Speed

// Configuracion para GPIO 22 CE y CE0 CSN con  velocidad SPI @ 4Mhz
//RF24 radio(RPI_V2_GPIO_P1_22, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ);

// Configuracion para GPIO 17 CE y CE0 CSN con velocidad SPI @ 8Mhz
RF24 radio(RPI_V2_GPIO_P1_11, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ);

// Configuracion generica RPi:
//RF24 radio(17,0);

/********************************/

// Direccion empleada para 1 nodo de comunicacion.
const uint64_t pipes[1] = {0xF0F0F0F0E1LL,};


int main(int argc, char** argv) {
    
    FILE *fd = popen("lsmod | grep desencriptador", "r");
    FILE *csv;
	
	system("rm logg.csv");
    system("rm /var/www/html/SdC/logg.csv");
    
    long int tiempo = 0;
    int dato, dato_desc, suma_asm, promedio;
    int hora, min, sec, dia, mes, anio;
    char* ch_antes = (char*)malloc(50*sizeof(char));
    char* ch_desp = (char*)malloc(50*sizeof(char));
    char* int_str = (char*)malloc(5*sizeof(char));
    char* temp = (char*)malloc(16*sizeof(char));
    char* date = (char*)malloc(255 * sizeof(char));
    int* muestras = (int*)malloc(10*sizeof(int));
    int muestras_cant = 0;
    char* token;
    memset(muestras,0,sizeof(muestras));
    memset(temp,0,sizeof(temp));
    
    printf("nRF24L01 -- INICIALIZACION \n");

    // Configuracion del modulo de radiofrecuencia 'radio'
    radio.begin();

    // Definimos el intervalo de tiempo entre reintentos de trasmision y cantidad de ellos
    radio.setRetries(15,15);
    // Impresion de los parametros de configuracion. DEBUG
    radio.printDetails();
    radio.enableDynamicPayloads();
    radio.setAutoAck(1);
    radio.setDataRate(RF24_1MBPS);
    radio.setPALevel(RF24_PA_MAX);
    radio.setChannel(76);

    printf("\n ************ Esperando datos ***********\n");

    /***********************************/
    // Habilidamos para lectura la region de memoria defina en el pipe
    radio.openReadingPipe(1,pipes[0]);

    // Habilitamos la recepcion de mensajes..
    radio.startListening();

    // Loop infinito
    while (1)
    {
        dato_desc = 0;
        FILE *in;
        
        // Compruebo si hay informacion reciente
        if ( radio.available() )
        {

            while(radio.available()) {
                radio.read( &dato, sizeof(int) );
            }
            
            // Convierto el dato (int) a string
            sprintf(int_str, "%d" , dato);
            
            printf("Dato ENCRIPTADO recibido: %d\n", dato);
            
            // Verifico si el modulo se encuentra cargado
            if(check_module())
            {
                // Si el modulo esta cargado, genero la llamada para
                // desencriptar la informacion.
                strcat(ch_antes,"echo ");
                strcat(ch_antes,int_str);
                strcat(ch_antes," > /dev/desencriptador");
                system(ch_antes);
                
                // Genero la llamada para obtener el dato del modulo
                strcat(ch_desp,"cat ");
                strcat(ch_desp,"/dev/desencriptador");
                printf("[MOD_ON] Dato DESENCRIPTADO: ");
                fflush(stdout);
                in=popen(ch_desp,"r");
                fread (temp, 1, sizeof(temp), in);
                
                token = strtok(temp,"e");
                token = strtok(token,"s");
                dato_desc = parseInt(token);
                printf("%d\n", dato_desc);
                    
                memset(ch_antes,0,sizeof(ch_antes));
                memset(ch_desp,0,sizeof(ch_desp));
                memset(temp,0,sizeof(temp));
                
                pclose(in);
                tiempo++;
                
                if(muestras_cant < 5)
                {
                    muestras[muestras_cant] = dato_desc;
                    muestras_cant++;
                }
                else
                {
                    csv=fopen("logg.csv","a");
                    memset(date,0,sizeof(date));
            
                    time_t now;
                    time(&now);
                    struct tm *local = localtime(&now);
                    hora = local->tm_hour;
                    min = local->tm_min;
                    sec = local->tm_sec;
                    dia = local->tm_mday;
                    mes = local->tm_mon + 1;
                    anio = local->tm_year + 1900;
                    sprintf(date, "%d-%d-%d %d:%d:%d", dia, mes, anio, hora, min, sec);
                    
                    suma_asm = suma(muestras[0], muestras[1], muestras[2], muestras[3], muestras[4]);
                    promedio = (int)(suma_asm/muestras_cant);
                    muestras_cant = 0;
                    printf("[PROMEDIO | %s] Valor: %d\n", date, promedio);
                    fflush(stdout);
                    fprintf(csv,"%d, %d\n",tiempo,promedio);
                    memset(muestras,0,sizeof(muestras));
                    fclose(csv);
                    system("sudo cp logg.csv /var/www/html/SdC/logg.csv");
                }
            }
            else
            {
                printf("[MOD_OFF] Desencriptador No Cargado -- \n");
            }
            printf("\n");
        
            delay(1025); //Delay para minimizar el tiempo de CPU de RPi
        }

    }

    return 0;
}


/* Verifico si el modulo se encuentra cargado en el sistema.
 * Devuelte 1 en caso de estar cargado, 0 en otro caso. */
int check_module()
{
    FILE *fd = popen("lsmod | grep desencriptador", "r");
    char buf[16];
    int estado;
    
    if (fread (buf, 1, sizeof(buf), fd) > 0)
    {
        estado = 1;
    }
    else
    {
        estado = 0;
    }
    pclose(fd);
    return estado;
}

int parseInt(char* mensaje)
{
	int x;
	int suma = 0;
	int len = strlen(mensaje);
	for (x = 0; x < len; x++)
	{
		int n = mensaje[len - (x + 1)] - '0';
		suma = suma + powInt(n, x);
	}
	return suma;
}

int powInt(int x, int y)
{
	int i;
	for(i = 0; i < y; i++)
	{
		x *= 10;
	}
	return x;
}

